import java.util.ArrayList;
import java.util.Arrays;

/**
 * Java Collections Practice 2.1
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity2_1 {

    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<Integer>();
        arrList.addAll(Arrays.asList(10,7,4,6,56,7,12,15,65,15,88,56,33));
        
        // Use the lastIndexOf() method to find the last index of the element with the given value
        int lastIndex = arrList.lastIndexOf(56);

        System.out.println(lastIndex);
    }

}