import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Java Collections Practice 3.2
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity3_2 {

    public static void main(String[] args) {
        ArrayList<Integer> idList = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5));
        ArrayList<String> nameList = new ArrayList<String>(Arrays.asList("Ajay", "Amit", "Kiran", "Rathina", "Srider"));
        
        // Initialize the map (we will assume the sizes are equal in both lists)
        HashMap<Integer, String> map = new HashMap<>();
        int index = 0;
        for (Integer id : idList) {
            map.put(id, nameList.get(index));
            index++;
        }

        System.out.println(map);
    }

}