import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Java Collections Practice 3.1
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity3_1 {

    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<Integer>(Arrays.asList(2,3,1,4,5,2,1,5,3));

        // Using the HashSet concrete implementation
        Set<Integer> set = new HashSet<Integer>();
        set.addAll(arrList);

        System.out.println(set.size());
    }

}