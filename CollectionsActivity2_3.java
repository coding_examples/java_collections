import java.util.PriorityQueue;

/**
 * Java Collections Practice 2.3
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity2_3 {

    public static void main(String[] args) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.add(10);
        pq.add(50);
        pq.add(30);

        // Peek in the queue. Peeking only retrieves the head.
        int head = pq.peek();
        System.out.println(head);

        // Poll this time. Polling retrieves and removes the head of the queue.
        head = pq.poll();
        System.out.println(head);

        // Peek again. This time, we should have a new head (the second element).
        head = pq.peek();
        System.out.println(head);
    }

}