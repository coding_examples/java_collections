import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Java Collections Practice 2.2
 * Copyright of Crio Pvt. Ltd.
 */
public class CollectionsActivity2_2 {

    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<Integer>();
        arrList.addAll(Arrays.asList(10,7,4,6,56,7,12,15,65,15,88,56,33));

        // Note that subList() returns List type, and not ArrayList type.
        List<Integer> list = arrList.subList(2, 5);

        // So if you use a variable of type ArrayList, you will see an error.
        //ArrayList<Integer> list = arrList.subList(2, 5);

        System.out.println(list);
    }

}